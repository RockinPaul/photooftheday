//
//  Parser.h
//  PhotoOfTheDay
//
//  Created by Pavel on 19.08.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Parser : NSObject <NSXMLParserDelegate>

@property (nonatomic, strong) NSXMLParser *parser;
@property (nonatomic, strong) NSMutableArray *feed; // Main feed data source.
@property (nonatomic, strong) NSMutableDictionary *item; // Single item in feed array.
@property (nonatomic, strong) NSMutableString *author; // user info about author.
@property (nonatomic, strong) NSMutableString *title; // image title.
@property (nonatomic, strong) NSMutableString *thumbnailLink; // link to feed version of image.
@property (nonatomic, strong) NSMutableString *imageLink; // link to detailed version of image.
@property (nonatomic, strong) NSString *currentElement;

- (void)parseFileAtURL:(NSString *)URL;

+ (Parser *)sharedInstance;

@end
