//
//  FeedCollectionViewController.h
//  PhotoOfTheDay
//
//  Created by Pavel on 19.08.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parser.h"
#import "Photo.h"
#import "ThumbnailCollectionViewCell.h"
#import "CacheController.h"
#import "DetailViewController.h"

@interface FeedCollectionViewController : UICollectionViewController

extern NSString *const URL;

@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) CacheController *cacheController;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end
