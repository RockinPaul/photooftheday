//
//  ThumbnailCollectionViewCell.m
//  PhotoOfTheDay
//
//  Created by Pavel on 19.08.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import "ThumbnailCollectionViewCell.h"

@implementation ThumbnailCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        [self.contentView addSubview:self.imageView];
    }
    return self;
}


- (void)prepareForReuse {
    
    [super prepareForReuse];
    // reset image property of imageView for reuse
    self.imageView.image = nil;
    // update frame position of subviews
    self.imageView.frame = self.contentView.bounds;
}

@end
