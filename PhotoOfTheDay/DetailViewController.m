//
//  DetailViewController.m
//  PhotoOfTheDay
//
//  Created by Pavel on 19.08.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createLayout];
    
    [self setContent];
}


#pragma mark - Content

- (void)setContent {
    
    CacheController *cacheController = [CacheController sharedInstance];
    self.authorLabel.text = self.photo.author;
    self.imageTitleLabel.text = self.photo.title;
    
    NSURL *imageURL = [NSURL URLWithString:self.photo.imageLink];
    
    UIImage *cachedImage = [cacheController getCachedImageForKey:[self.photo.imageLink stringByReplacingOccurrencesOfString:@"/" withString:@""]];
    
    if (cachedImage) {
        self.imageView.image = cachedImage;
        NSLog(@"LOADING FROM CACHE");
    } else {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:imageURL];
            UIImage *image = [UIImage imageWithData:data];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.imageView.image = image;
                if (!image) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Unable to load image"
                                                                        message:@"Image not available.\nPlease check the internet connection."
                                                                       delegate:self
                                                              cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alertView show];
                } else {
                    [cacheController cacheImage:image forKey:[self.photo.imageLink stringByReplacingOccurrencesOfString:@"/" withString:@""]];
                }
            });
        });
    }

    
}


#pragma mark - Layout
- (void)createLayout {
    
    self.footerView = [UIView new];
    self.footerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.footerView setBackgroundColor:[UIColor whiteColor]];
    [self.footerView setAlpha:0.5];
    
    self.authorLabel = [UILabel new];
    self.authorLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.authorLabel setFont:[UIFont fontWithName:@"Helvetica" size:16.0]];
    [self.authorLabel setTextColor:[UIColor blueColor]];
    
    self.imageTitleLabel = [UILabel new];
    self.imageTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.imageTitleLabel setFont:[UIFont fontWithName:@"Helvetica" size:16.0]];
    [self.imageTitleLabel setTextColor:[UIColor blueColor]];
    
    self.imageView = [UIImageView new];
    self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
    
//    // In case if image is not cached and internet connection is unavailable.
//    UILabel *alertLabel = [[UILabel alloc] initWithFrame:self.view.bounds];
//    alertLabel.text = @"Image not available.\nPlease check the internet connection.";
//    alertLabel.numberOfLines = 2;
//    alertLabel.textColor = [UIColor whiteColor];
//    alertLabel.textAlignment = NSTextAlignmentCenter;
//    alertLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
//    [detailVC.view addSubview:alertLabel];
    
    [self.view addSubview:self.imageView];
    [self.view addSubview:self.footerView];
    [self.footerView addSubview:self.authorLabel];
    [self.footerView addSubview:self.imageTitleLabel];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_footerView, _authorLabel, _imageTitleLabel, _imageView);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(>=64)-[_imageView(==200)]-(<=60)-[_footerView(>=40,<=60)]-0-|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(>=10)-[_imageView(==300)]-(>=10)-|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[_footerView(>=320)]-0-|" options:0 metrics:nil views:views]];
    [self.footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[_authorLabel]-10-[_imageTitleLabel]-|" options:0 metrics:nil views:views]];
    
    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.authorLabel
                                                               attribute:NSLayoutAttributeCenterX
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.footerView
                                                               attribute:NSLayoutAttributeCenterX
                                                              multiplier:1.0
                                                                constant:0]];
    
    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.imageTitleLabel
                                                                attribute:NSLayoutAttributeCenterX
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.footerView
                                                                attribute:NSLayoutAttributeCenterX
                                                               multiplier:1.0
                                                                 constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                                attribute:NSLayoutAttributeCenterY
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.view
                                                                attribute:NSLayoutAttributeCenterY
                                                               multiplier:1.0
                                                                 constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                                attribute:NSLayoutAttributeCenterX
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.view
                                                                attribute:NSLayoutAttributeCenterX
                                                               multiplier:1.0
                                                                 constant:0]];

}

@end
