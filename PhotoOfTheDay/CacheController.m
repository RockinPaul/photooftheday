//
//  CacheController.m
//  PhotoOfTheDay
//
//  Created by Pavel on 19.08.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import "CacheController.h"

@implementation CacheController

// First of all we need to specify storage path.
- (instancetype)init {
    self = [super init];
    if (self) {
        self.path = NSSearchPathForDirectoriesInDomains(NSPicturesDirectory, NSUserDomainMask, YES);
        self.documentDirectory = [self.path objectAtIndex:0];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        // iOS 8 feature: directory can not exists.
        if (![fileManager fileExistsAtPath:self.documentDirectory]) {
            NSLog(@"DOESN'T EXISTS");
            [fileManager createDirectoryAtPath:self.documentDirectory withIntermediateDirectories:NO attributes:nil error:&error];
        }
    }
    return self;
}

#pragma mark - cacheImage
// Cache image to internal storage.
- (void)cacheImage:(UIImage *)image forKey:(NSString *)key {
    NSData *imageData = UIImagePNGRepresentation(image);
    NSString *imagePath = [self.documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", key]];
    NSError * error = nil;
    
    if (![imageData writeToFile:imagePath options:NSDataWritingAtomic error:&error]) {
        NSLog(@"Failed to cache image data to disk: %@", error);
    } else {
        NSLog(@"the cachedImagedPath is %@", imagePath);
    }
}


#pragma mark - getCachedImageForKey
// Get cached image from internal storage.
- (UIImage *)getCachedImageForKey:(NSString *)key {
    
    NSString *imagePath = [self.documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", key]];
    UIImage *retrievedImage = [UIImage imageWithContentsOfFile:imagePath];
    return retrievedImage;
}


#pragma mark - cachePlaces

- (void)cachePhotos:(NSArray *)photos {
    NSString *photosPath = [self.documentDirectory stringByAppendingPathComponent:@"photos"];
    [NSKeyedArchiver archiveRootObject:photos toFile:photosPath];
}


#pragma mark - getCachedPlaces

- (NSArray *)getCachedPhotos {
    NSString *photosPath = [self.documentDirectory stringByAppendingPathComponent:@"photos"];
    NSArray *photos = [NSKeyedUnarchiver unarchiveObjectWithFile:photosPath];
    NSLog(@"%@", photosPath);
    return photos;
}


#pragma mark - Shared Instance

+ (CacheController *)sharedInstance {
    static dispatch_once_t pred;
    static CacheController *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[CacheController alloc] init];
    });
    return sharedInstance;
}

@end
