//
//  Parser.m
//  PhotoOfTheDay
//
//  Created by Pavel on 19.08.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import "Parser.h"

@implementation Parser

#pragma mark - Parse file at URL
// Setting up the parser.
- (void)parseFileAtURL:(NSString *)URL {
    
    NSURL *xmlURL = [NSURL URLWithString:URL];
    NSData *data = [NSData dataWithContentsOfURL:xmlURL];
    self.parser = [[NSXMLParser alloc] initWithData:data];
    [self.parser setDelegate:self];
    [self.parser setShouldProcessNamespaces:NO];
    [self.parser setShouldReportNamespacePrefixes:NO];
    [self.parser setShouldResolveExternalEntities:NO];
    [self.parser parse];
}


#pragma mark - Parser did start document
// Parsing start point.
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    self.feed = [[NSMutableArray alloc] init];
}


#pragma mark - Errors observer
// Error's handler with alert in modal window.
- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    NSString *errorString = [NSString stringWithFormat:@"Unable to download picture: %li", (long)[parseError code]];
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error loading content"
                                                         message:errorString
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [errorAlert show];
}


#pragma mark - Parser's start element
// Init temp values.
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    self.currentElement = [elementName copy];
    if ([elementName isEqualToString:@"item"]) {
        self.item = [[NSMutableDictionary alloc] init];
        self.author = [[NSMutableString alloc] init];
        self.title = [[NSMutableString alloc] init];
        self.imageLink = [[NSMutableString alloc] init];
        self.thumbnailLink = [[NSMutableString alloc] init];
    } else if ([elementName isEqualToString:@"media:thumbnail"]) {
        self.thumbnailLink = [attributeDict valueForKey:@"url"];
    } else if ([elementName isEqualToString:@"media:content"]) {
        self.imageLink = [attributeDict valueForKey:@"url"];
    }
}


#pragma mark - Parser's end element
// Saving parsed data to feed array.
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        [self.item setObject:self.author forKey:@"author"];
        [self.item setObject:self.title forKey:@"title"];
        [self.item setObject:self.imageLink forKey:@"imageLink"];
        [self.item setObject:self.thumbnailLink forKey:@"thumbnailLink"];
        [self.feed addObject:self.item];
    }
}


#pragma mark - Images info
// Image info parsing.
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([self.currentElement isEqualToString:@"author"]) {
        [self.author appendString:string];
    } else if ([self.currentElement isEqualToString:@"title"]) {
        [self.title appendString:string];
    }
}


#pragma mark - Parser did end document
// At the ending of the parsing.
- (void)parserDidEndDocument:(NSXMLParser *)parser {
    // Notification to FeedCollectionViewController.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"parserDidEndDocument" object:self];
    NSLog(@"END");
}


#pragma mark - Shared Instance
// Singleton implementation
+ (Parser *)sharedInstance {
    static dispatch_once_t pred;
    static Parser *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[Parser alloc] init];
    });
    return sharedInstance;
}

@end
