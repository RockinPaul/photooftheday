//
//  Photo.m
//  PhotoOfTheDay
//
//  Created by Pavel on 19.08.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import "Photo.h"

@implementation Photo

#pragma mark - Initialization

- (Photo *)initWithDictionary:(NSDictionary *)dictionary {
    Photo *photo = [Photo new];
    photo.title = [dictionary objectForKey:@"title"];
    photo.author = [dictionary objectForKey:@"author"];
    photo.imageLink = [dictionary objectForKey:@"imageLink"];
    photo.thumbnailLink = [dictionary objectForKey:@"thumbnailLink"];
    return photo;
}


#pragma mark - NSCoding implementation

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.author = [aDecoder decodeObjectForKey:@"author"];
        self.imageLink = [aDecoder decodeObjectForKey:@"imageLink"];
        self.thumbnailLink = [aDecoder decodeObjectForKey:@"thumbnailLink"];
    }
    return self;
}
                         
-(void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.title forKey:@"title"];
    [encoder encodeObject:self.author forKey:@"author"];
    [encoder encodeObject:self.imageLink forKey:@"imageLink"];
    [encoder encodeObject:self.thumbnailLink forKey:@"thumbnailLink"];
}

@end
