//
//  CacheController.h
//  PhotoOfTheDay
//
//  Created by Pavel on 19.08.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CacheController : NSObject

@property (nonatomic, strong) NSArray *path;
@property (nonatomic, strong) NSString *documentDirectory;

// Image caching.
- (void)cacheImage:(UIImage*)image forKey:(NSString *)key;
- (UIImage *)getCachedImageForKey:(NSString *)key;

- (void)cachePhotos:(NSArray *)photos;
- (NSArray *)getCachedPhotos;

+ (CacheController *)sharedInstance;

@end
