//
//  DetailViewController.h
//  PhotoOfTheDay
//
//  Created by Pavel on 19.08.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Photo.h"
#import "CacheController.h"

@interface DetailViewController : UIViewController

@property (nonatomic, strong) Photo *photo;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *authorLabel;
@property (nonatomic, strong) UILabel *imageTitleLabel;
@property (nonatomic, strong) UIView *footerView;

@end
