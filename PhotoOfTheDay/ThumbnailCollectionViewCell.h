//
//  ThumbnailCollectionViewCell.h
//  PhotoOfTheDay
//
//  Created by Pavel on 19.08.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThumbnailCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;

@end
