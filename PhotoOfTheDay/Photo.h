//
//  Photo.h
//  PhotoOfTheDay
//
//  Created by Pavel on 19.08.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"

@interface Photo : NSObject <NSCoding>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *author;
@property (nonatomic, strong) NSString *imageLink;
@property (nonatomic, strong) NSString *thumbnailLink;

- (Photo *)initWithDictionary:(NSDictionary *)dictionary;

@end
