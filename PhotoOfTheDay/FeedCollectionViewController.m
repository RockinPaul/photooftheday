//
//  FeedCollectionViewController.m
//  PhotoOfTheDay
//
//  Created by Pavel on 19.08.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import "FeedCollectionViewController.h"


@implementation FeedCollectionViewController

NSString *const URL = @"http://fotki.yandex.ru/calendar/rss2";
static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Push-to-refresh implementation
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(startRefresh)
                  forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(documentParsed)
                                                 name:@"parserDidEndDocument"
                                               object:nil];
    self.cacheController = [CacheController sharedInstance];
    
    // Register cell classes
    [self.collectionView registerClass:[ThumbnailCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    // Do any additional setup after loading the view.
    
    CacheController *cacheController = [CacheController sharedInstance];
    self.photos = [cacheController getCachedPhotos];
    if (!self.photos) {
        Parser *parser = [Parser sharedInstance];
        [parser parseFileAtURL:URL];
    }
}


#pragma mark - Pull-to-refresh observer

- (void)startRefresh {
    Parser *parser = [Parser sharedInstance];
    [parser parseFileAtURL:URL];
}


#pragma mark - Document parsed

- (void)documentParsed {
    
    [self.refreshControl endRefreshing]; // End refreshing on pull-to-refresh
    CacheController *cacheController = [CacheController sharedInstance];
    Parser *parser = [Parser sharedInstance];
    NSMutableArray *tempArray = [NSMutableArray new];
    Photo *photo = nil;
    for (int i = 0; i < parser.feed.count; i++) {
        NSDictionary *dict = parser.feed[i];
        NSLog(@"%@", [dict objectForKey:@"title"]);
        photo = [[Photo alloc] initWithDictionary:dict];
        [tempArray addObject:photo];
    }
    self.photos = [NSArray arrayWithArray:tempArray];
    [cacheController cachePhotos:self.photos];
    [self.collectionView reloadData];
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ThumbnailCollectionViewCell *cell = (ThumbnailCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    
    cell.imageView.image = [UIImage imageNamed:@"image_waiting"];
    Photo *photo = [self.photos objectAtIndex:indexPath.row];
    NSURL *imageURL = [NSURL URLWithString:photo.thumbnailLink];
    
    UIImage *cachedImage = [self.cacheController getCachedImageForKey:[photo.thumbnailLink stringByReplacingOccurrencesOfString:@"/" withString:@""]];
    
    if (cachedImage) {
        cell.imageView.image = cachedImage;
        NSLog(@"LOADING FROM CACHE");
    } else {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:imageURL];
            UIImage *image = [UIImage imageWithData:data];
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.imageView.image = image;
                [self.cacheController cacheImage:image forKey:[photo.thumbnailLink stringByReplacingOccurrencesOfString:@"/" withString:@""]];
            });
        });
    }
    return cell;
}


#pragma mark <UICollectionViewDelegate>

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    DetailViewController *detailVC = [DetailViewController new];
    Photo *photo = self.photos[indexPath.row];
    detailVC.photo = photo;
    
    [self.navigationController pushViewController:detailVC animated:YES];
    
    return YES;
}

@end
